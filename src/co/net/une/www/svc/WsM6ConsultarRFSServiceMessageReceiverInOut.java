
/**
 * WsM6ConsultarRFSServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WsM6ConsultarRFSServiceMessageReceiverInOut message receiver
        */

        public class WsM6ConsultarRFSServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WsM6ConsultarRFSServiceSkeleton skel = (WsM6ConsultarRFSServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("M6ConsultarRFS".equals(methodName)){
                
                co.net.une.www.ncainvm6.M6ConsultarRFS m6ConsultarRFS11 = null;
	                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedParam =
                                                             (co.net.une.www.ncainvm6.M6ConsultarRFS)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.ncainvm6.M6ConsultarRFS.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               m6ConsultarRFS11 =
                                                   
                                                   
                                                           wrapM6ConsultarRFS(
                                                       
                                                        

                                                        
                                                       skel.M6ConsultarRFS(
                                                            
                                                                getFechaSolicitud(wrappedParam)
                                                            ,
                                                                getDireccion(wrappedParam)
                                                            ,
                                                                getOfertaEconomica(wrappedParam)
                                                            ,
                                                                getDetalleRespuesta(wrappedParam)
                                                            ,
                                                                getListaRFS(wrappedParam)
                                                            ,
                                                                getListaRFSNoExistentes(wrappedParam)
                                                            ,
                                                                getAccesos(wrappedParam)
                                                            ,
                                                                getNotas(wrappedParam)
                                                            ,
                                                                getAtributos(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), m6ConsultarRFS11, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.ncainvm6.M6ConsultarRFS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.ncainvm6.M6ConsultarRFS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.ncainvm6.M6ConsultarRFS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.ncainvm6.M6ConsultarRFS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.ncainvm6.UTCDate getFechaSolicitud(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getFechaSolicitud();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Direcciontype getDireccion(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getDireccion();
                            
                        }
                     

                        private java.lang.String getOfertaEconomica(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getOfertaEconomica();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Detallerespuestatype getDetalleRespuesta(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getDetalleRespuesta();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listarfstype getListaRFS(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getListaRFS();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listarfsnoexistentestype getListaRFSNoExistentes(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getListaRFSNoExistentes();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listaaccesosrfstype getAccesos(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getAccesos();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listanotastype getNotas(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getNotas();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listaatributostype getAtributos(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                        
                                return wrappedType.getM6ConsultarRFS().getAtributos();
                            
                        }
                     
                        private co.net.une.www.ncainvm6.M6ConsultarRFSType getM6ConsultarRFS(
                        co.net.une.www.ncainvm6.M6ConsultarRFS wrappedType){
                            return wrappedType.getM6ConsultarRFS();
                        }
                        
                        
                    
                         private co.net.une.www.ncainvm6.M6ConsultarRFS wrapM6ConsultarRFS(
                            co.net.une.www.ncainvm6.M6ConsultarRFSType innerType){
                                co.net.une.www.ncainvm6.M6ConsultarRFS wrappedElement = new co.net.une.www.ncainvm6.M6ConsultarRFS();
                                wrappedElement.setM6ConsultarRFS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.ncainvm6.M6ConsultarRFS.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.M6ConsultarRFS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.ncainvm6.M6ConsultarRFS.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.M6ConsultarRFS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    