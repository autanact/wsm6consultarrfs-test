
/**
 * Equipotype.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.ncainvm6;
            

            /**
            *  Equipotype bean class
            */
        
        public  class Equipotype
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = equipotype
                Namespace URI = http://www.une.net.co/ncaInvM6
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/ncaInvM6")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Secuencia
                        */

                        
                                    protected java.lang.String localSecuencia ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSecuenciaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSecuencia(){
                               return localSecuencia;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Secuencia
                               */
                               public void setSecuencia(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localSecuenciaTracker = true;
                                       } else {
                                          localSecuenciaTracker = true;
                                              
                                       }
                                   
                                            this.localSecuencia=param;
                                    

                               }
                            

                        /**
                        * field for IdLocalizacion
                        */

                        
                                    protected java.lang.String localIdLocalizacion ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdLocalizacionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIdLocalizacion(){
                               return localIdLocalizacion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IdLocalizacion
                               */
                               public void setIdLocalizacion(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localIdLocalizacionTracker = true;
                                       } else {
                                          localIdLocalizacionTracker = false;
                                              
                                       }
                                   
                                            this.localIdLocalizacion=param;
                                    

                               }
                            

                        /**
                        * field for IdUneEquipoPadre
                        */

                        
                                    protected java.lang.String localIdUneEquipoPadre ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIdUneEquipoPadre(){
                               return localIdUneEquipoPadre;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IdUneEquipoPadre
                               */
                               public void setIdUneEquipoPadre(java.lang.String param){
                            
                                            this.localIdUneEquipoPadre=param;
                                    

                               }
                            

                        /**
                        * field for IdUneEquipo
                        */

                        
                                    protected java.lang.String localIdUneEquipo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIdUneEquipo(){
                               return localIdUneEquipo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IdUneEquipo
                               */
                               public void setIdUneEquipo(java.lang.String param){
                            
                                            this.localIdUneEquipo=param;
                                    

                               }
                            

                        /**
                        * field for AliasEquipo
                        */

                        
                                    protected java.lang.String localAliasEquipo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAliasEquipo(){
                               return localAliasEquipo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AliasEquipo
                               */
                               public void setAliasEquipo(java.lang.String param){
                            
                                            this.localAliasEquipo=param;
                                    

                               }
                            

                        /**
                        * field for TipoEquipo
                        */

                        
                                    protected java.lang.String localTipoEquipo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTipoEquipo(){
                               return localTipoEquipo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TipoEquipo
                               */
                               public void setTipoEquipo(java.lang.String param){
                            
                                            this.localTipoEquipo=param;
                                    

                               }
                            

                        /**
                        * field for DescripcionEquipo
                        */

                        
                                    protected java.lang.String localDescripcionEquipo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescripcionEquipoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescripcionEquipo(){
                               return localDescripcionEquipo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DescripcionEquipo
                               */
                               public void setDescripcionEquipo(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDescripcionEquipoTracker = true;
                                       } else {
                                          localDescripcionEquipoTracker = true;
                                              
                                       }
                                   
                                            this.localDescripcionEquipo=param;
                                    

                               }
                            

                        /**
                        * field for CanalEquipo
                        */

                        
                                    protected java.lang.String localCanalEquipo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCanalEquipoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCanalEquipo(){
                               return localCanalEquipo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CanalEquipo
                               */
                               public void setCanalEquipo(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCanalEquipoTracker = true;
                                       } else {
                                          localCanalEquipoTracker = true;
                                              
                                       }
                                   
                                            this.localCanalEquipo=param;
                                    

                               }
                            

                        /**
                        * field for Tecnologia
                        */

                        
                                    protected java.lang.String localTecnologia ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTecnologiaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTecnologia(){
                               return localTecnologia;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tecnologia
                               */
                               public void setTecnologia(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localTecnologiaTracker = true;
                                       } else {
                                          localTecnologiaTracker = true;
                                              
                                       }
                                   
                                            this.localTecnologia=param;
                                    

                               }
                            

                        /**
                        * field for UmbralFalla
                        */

                        
                                    protected java.lang.String localUmbralFalla ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUmbralFallaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUmbralFalla(){
                               return localUmbralFalla;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UmbralFalla
                               */
                               public void setUmbralFalla(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localUmbralFallaTracker = true;
                                       } else {
                                          localUmbralFallaTracker = true;
                                              
                                       }
                                   
                                            this.localUmbralFalla=param;
                                    

                               }
                            

                        /**
                        * field for Relevante
                        */

                        
                                    protected java.lang.String localRelevante ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelevanteTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRelevante(){
                               return localRelevante;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Relevante
                               */
                               public void setRelevante(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localRelevanteTracker = true;
                                       } else {
                                          localRelevanteTracker = true;
                                              
                                       }
                                   
                                            this.localRelevante=param;
                                    

                               }
                            

                        /**
                        * field for DireccionElementoRed
                        */

                        
                                    protected java.lang.String localDireccionElementoRed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDireccionElementoRedTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDireccionElementoRed(){
                               return localDireccionElementoRed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionElementoRed
                               */
                               public void setDireccionElementoRed(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDireccionElementoRedTracker = true;
                                       } else {
                                          localDireccionElementoRedTracker = true;
                                              
                                       }
                                   
                                            this.localDireccionElementoRed=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       Equipotype.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/ncaInvM6");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":equipotype",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "equipotype",
                           xmlWriter);
                   }

               
                   }
                if (localSecuenciaTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Secuencia", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Secuencia");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Secuencia");
                                    }
                                

                                          if (localSecuencia==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSecuencia);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIdLocalizacionTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"IdLocalizacion", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"IdLocalizacion");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("IdLocalizacion");
                                    }
                                

                                          if (localIdLocalizacion==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("IdLocalizacion cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIdLocalizacion);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"IdUneEquipoPadre", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"IdUneEquipoPadre");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("IdUneEquipoPadre");
                                    }
                                

                                          if (localIdUneEquipoPadre==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("IdUneEquipoPadre cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIdUneEquipoPadre);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"IdUneEquipo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"IdUneEquipo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("IdUneEquipo");
                                    }
                                

                                          if (localIdUneEquipo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("IdUneEquipo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIdUneEquipo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"AliasEquipo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"AliasEquipo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("AliasEquipo");
                                    }
                                

                                          if (localAliasEquipo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("AliasEquipo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAliasEquipo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"TipoEquipo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"TipoEquipo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("TipoEquipo");
                                    }
                                

                                          if (localTipoEquipo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("TipoEquipo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTipoEquipo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                              if (localDescripcionEquipoTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"DescripcionEquipo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"DescripcionEquipo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("DescripcionEquipo");
                                    }
                                

                                          if (localDescripcionEquipo==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescripcionEquipo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCanalEquipoTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CanalEquipo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CanalEquipo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CanalEquipo");
                                    }
                                

                                          if (localCanalEquipo==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCanalEquipo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTecnologiaTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Tecnologia", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Tecnologia");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Tecnologia");
                                    }
                                

                                          if (localTecnologia==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTecnologia);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUmbralFallaTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"UmbralFalla", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"UmbralFalla");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("UmbralFalla");
                                    }
                                

                                          if (localUmbralFalla==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUmbralFalla);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRelevanteTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"Relevante", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"Relevante");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("Relevante");
                                    }
                                

                                          if (localRelevante==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRelevante);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDireccionElementoRedTracker){
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"DireccionElementoRed", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"DireccionElementoRed");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("DireccionElementoRed");
                                    }
                                

                                          if (localDireccionElementoRed==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDireccionElementoRed);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localSecuenciaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Secuencia"));
                                 
                                         elementList.add(localSecuencia==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSecuencia));
                                    } if (localIdLocalizacionTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "IdLocalizacion"));
                                 
                                        if (localIdLocalizacion != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIdLocalizacion));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("IdLocalizacion cannot be null!!");
                                        }
                                    }
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "IdUneEquipoPadre"));
                                 
                                        if (localIdUneEquipoPadre != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIdUneEquipoPadre));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("IdUneEquipoPadre cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "IdUneEquipo"));
                                 
                                        if (localIdUneEquipo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIdUneEquipo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("IdUneEquipo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "AliasEquipo"));
                                 
                                        if (localAliasEquipo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAliasEquipo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("AliasEquipo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "TipoEquipo"));
                                 
                                        if (localTipoEquipo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTipoEquipo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("TipoEquipo cannot be null!!");
                                        }
                                     if (localDescripcionEquipoTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "DescripcionEquipo"));
                                 
                                         elementList.add(localDescripcionEquipo==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescripcionEquipo));
                                    } if (localCanalEquipoTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "CanalEquipo"));
                                 
                                         elementList.add(localCanalEquipo==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCanalEquipo));
                                    } if (localTecnologiaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Tecnologia"));
                                 
                                         elementList.add(localTecnologia==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTecnologia));
                                    } if (localUmbralFallaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "UmbralFalla"));
                                 
                                         elementList.add(localUmbralFalla==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUmbralFalla));
                                    } if (localRelevanteTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Relevante"));
                                 
                                         elementList.add(localRelevante==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRelevante));
                                    } if (localDireccionElementoRedTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "DireccionElementoRed"));
                                 
                                         elementList.add(localDireccionElementoRed==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDireccionElementoRed));
                                    }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Equipotype parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Equipotype object =
                new Equipotype();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"equipotype".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Equipotype)co.net.une.www.ncainvm6.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Secuencia").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSecuencia(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","IdLocalizacion").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIdLocalizacion(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","IdUneEquipoPadre").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIdUneEquipoPadre(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","IdUneEquipo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIdUneEquipo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","AliasEquipo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAliasEquipo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","TipoEquipo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTipoEquipo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","DescripcionEquipo").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescripcionEquipo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","CanalEquipo").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCanalEquipo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Tecnologia").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTecnologia(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","UmbralFalla").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUmbralFalla(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Relevante").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRelevante(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","DireccionElementoRed").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDireccionElementoRed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          