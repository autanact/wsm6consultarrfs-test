
/**
 * M6ConsultarRFSType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.ncainvm6;
            

            /**
            *  M6ConsultarRFSType bean class
            */
        
        public  class M6ConsultarRFSType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = M6ConsultarRFSType
                Namespace URI = http://www.une.net.co/ncaInvM6
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/ncaInvM6")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for FechaSolicitud
                        */

                        
                                    protected co.net.une.www.ncainvm6.UTCDate localFechaSolicitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.UTCDate
                           */
                           public  co.net.une.www.ncainvm6.UTCDate getFechaSolicitud(){
                               return localFechaSolicitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FechaSolicitud
                               */
                               public void setFechaSolicitud(co.net.une.www.ncainvm6.UTCDate param){
                            
                                            this.localFechaSolicitud=param;
                                    

                               }
                            

                        /**
                        * field for Direccion
                        */

                        
                                    protected co.net.une.www.ncainvm6.Direcciontype localDireccion ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Direcciontype
                           */
                           public  co.net.une.www.ncainvm6.Direcciontype getDireccion(){
                               return localDireccion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Direccion
                               */
                               public void setDireccion(co.net.une.www.ncainvm6.Direcciontype param){
                            
                                            this.localDireccion=param;
                                    

                               }
                            

                        /**
                        * field for OfertaEconomica
                        */

                        
                                    protected java.lang.String localOfertaEconomica ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOfertaEconomica(){
                               return localOfertaEconomica;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OfertaEconomica
                               */
                               public void setOfertaEconomica(java.lang.String param){
                            
                                            this.localOfertaEconomica=param;
                                    

                               }
                            

                        /**
                        * field for DetalleRespuesta
                        */

                        
                                    protected co.net.une.www.ncainvm6.Detallerespuestatype localDetalleRespuesta ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDetalleRespuestaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Detallerespuestatype
                           */
                           public  co.net.une.www.ncainvm6.Detallerespuestatype getDetalleRespuesta(){
                               return localDetalleRespuesta;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DetalleRespuesta
                               */
                               public void setDetalleRespuesta(co.net.une.www.ncainvm6.Detallerespuestatype param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDetalleRespuestaTracker = true;
                                       } else {
                                          localDetalleRespuestaTracker = true;
                                              
                                       }
                                   
                                            this.localDetalleRespuesta=param;
                                    

                               }
                            

                        /**
                        * field for ListaRFS
                        */

                        
                                    protected co.net.une.www.ncainvm6.Listarfstype localListaRFS ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localListaRFSTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Listarfstype
                           */
                           public  co.net.une.www.ncainvm6.Listarfstype getListaRFS(){
                               return localListaRFS;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ListaRFS
                               */
                               public void setListaRFS(co.net.une.www.ncainvm6.Listarfstype param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localListaRFSTracker = true;
                                       } else {
                                          localListaRFSTracker = true;
                                              
                                       }
                                   
                                            this.localListaRFS=param;
                                    

                               }
                            

                        /**
                        * field for ListaRFSNoExistentes
                        */

                        
                                    protected co.net.une.www.ncainvm6.Listarfsnoexistentestype localListaRFSNoExistentes ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localListaRFSNoExistentesTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Listarfsnoexistentestype
                           */
                           public  co.net.une.www.ncainvm6.Listarfsnoexistentestype getListaRFSNoExistentes(){
                               return localListaRFSNoExistentes;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ListaRFSNoExistentes
                               */
                               public void setListaRFSNoExistentes(co.net.une.www.ncainvm6.Listarfsnoexistentestype param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localListaRFSNoExistentesTracker = true;
                                       } else {
                                          localListaRFSNoExistentesTracker = true;
                                              
                                       }
                                   
                                            this.localListaRFSNoExistentes=param;
                                    

                               }
                            

                        /**
                        * field for Accesos
                        */

                        
                                    protected co.net.une.www.ncainvm6.Listaaccesosrfstype localAccesos ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccesosTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Listaaccesosrfstype
                           */
                           public  co.net.une.www.ncainvm6.Listaaccesosrfstype getAccesos(){
                               return localAccesos;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Accesos
                               */
                               public void setAccesos(co.net.une.www.ncainvm6.Listaaccesosrfstype param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localAccesosTracker = true;
                                       } else {
                                          localAccesosTracker = true;
                                              
                                       }
                                   
                                            this.localAccesos=param;
                                    

                               }
                            

                        /**
                        * field for Notas
                        */

                        
                                    protected co.net.une.www.ncainvm6.Listanotastype localNotas ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNotasTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Listanotastype
                           */
                           public  co.net.une.www.ncainvm6.Listanotastype getNotas(){
                               return localNotas;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Notas
                               */
                               public void setNotas(co.net.une.www.ncainvm6.Listanotastype param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNotasTracker = true;
                                       } else {
                                          localNotasTracker = true;
                                              
                                       }
                                   
                                            this.localNotas=param;
                                    

                               }
                            

                        /**
                        * field for Atributos
                        */

                        
                                    protected co.net.une.www.ncainvm6.Listaatributostype localAtributos ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAtributosTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.ncainvm6.Listaatributostype
                           */
                           public  co.net.une.www.ncainvm6.Listaatributostype getAtributos(){
                               return localAtributos;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Atributos
                               */
                               public void setAtributos(co.net.une.www.ncainvm6.Listaatributostype param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localAtributosTracker = true;
                                       } else {
                                          localAtributosTracker = true;
                                              
                                       }
                                   
                                            this.localAtributos=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       M6ConsultarRFSType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/ncaInvM6");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":M6ConsultarRFSType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "M6ConsultarRFSType",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localFechaSolicitud==null){
                                                 throw new org.apache.axis2.databinding.ADBException("FechaSolicitud cannot be null!!");
                                            }
                                           localFechaSolicitud.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","FechaSolicitud"),
                                               factory,xmlWriter);
                                        
                                            if (localDireccion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Direccion cannot be null!!");
                                            }
                                           localDireccion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Direccion"),
                                               factory,xmlWriter);
                                        
                                    namespace = "http://www.une.net.co/ncaInvM6";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"OfertaEconomica", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"OfertaEconomica");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("OfertaEconomica");
                                    }
                                

                                          if (localOfertaEconomica==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("OfertaEconomica cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOfertaEconomica);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                              if (localDetalleRespuestaTracker){
                                    if (localDetalleRespuesta==null){

                                            java.lang.String namespace2 = "http://www.une.net.co/ncaInvM6";

                                        if (! namespace2.equals("")) {
                                            java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                            if (prefix2 == null) {
                                                prefix2 = generatePrefix(namespace2);

                                                xmlWriter.writeStartElement(prefix2,"DetalleRespuesta", namespace2);
                                                xmlWriter.writeNamespace(prefix2, namespace2);
                                                xmlWriter.setPrefix(prefix2, namespace2);

                                            } else {
                                                xmlWriter.writeStartElement(namespace2,"DetalleRespuesta");
                                            }

                                        } else {
                                            xmlWriter.writeStartElement("DetalleRespuesta");
                                        }


                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localDetalleRespuesta.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","DetalleRespuesta"),
                                        factory,xmlWriter);
                                    }
                                } if (localListaRFSTracker){
                                    if (localListaRFS==null){

                                            java.lang.String namespace2 = "http://www.une.net.co/ncaInvM6";

                                        if (! namespace2.equals("")) {
                                            java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                            if (prefix2 == null) {
                                                prefix2 = generatePrefix(namespace2);

                                                xmlWriter.writeStartElement(prefix2,"ListaRFS", namespace2);
                                                xmlWriter.writeNamespace(prefix2, namespace2);
                                                xmlWriter.setPrefix(prefix2, namespace2);

                                            } else {
                                                xmlWriter.writeStartElement(namespace2,"ListaRFS");
                                            }

                                        } else {
                                            xmlWriter.writeStartElement("ListaRFS");
                                        }


                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localListaRFS.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","ListaRFS"),
                                        factory,xmlWriter);
                                    }
                                } if (localListaRFSNoExistentesTracker){
                                    if (localListaRFSNoExistentes==null){

                                            java.lang.String namespace2 = "http://www.une.net.co/ncaInvM6";

                                        if (! namespace2.equals("")) {
                                            java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                            if (prefix2 == null) {
                                                prefix2 = generatePrefix(namespace2);

                                                xmlWriter.writeStartElement(prefix2,"ListaRFSNoExistentes", namespace2);
                                                xmlWriter.writeNamespace(prefix2, namespace2);
                                                xmlWriter.setPrefix(prefix2, namespace2);

                                            } else {
                                                xmlWriter.writeStartElement(namespace2,"ListaRFSNoExistentes");
                                            }

                                        } else {
                                            xmlWriter.writeStartElement("ListaRFSNoExistentes");
                                        }


                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localListaRFSNoExistentes.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","ListaRFSNoExistentes"),
                                        factory,xmlWriter);
                                    }
                                } if (localAccesosTracker){
                                    if (localAccesos==null){

                                            java.lang.String namespace2 = "http://www.une.net.co/ncaInvM6";

                                        if (! namespace2.equals("")) {
                                            java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                            if (prefix2 == null) {
                                                prefix2 = generatePrefix(namespace2);

                                                xmlWriter.writeStartElement(prefix2,"Accesos", namespace2);
                                                xmlWriter.writeNamespace(prefix2, namespace2);
                                                xmlWriter.setPrefix(prefix2, namespace2);

                                            } else {
                                                xmlWriter.writeStartElement(namespace2,"Accesos");
                                            }

                                        } else {
                                            xmlWriter.writeStartElement("Accesos");
                                        }


                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localAccesos.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Accesos"),
                                        factory,xmlWriter);
                                    }
                                } if (localNotasTracker){
                                    if (localNotas==null){

                                            java.lang.String namespace2 = "http://www.une.net.co/ncaInvM6";

                                        if (! namespace2.equals("")) {
                                            java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                            if (prefix2 == null) {
                                                prefix2 = generatePrefix(namespace2);

                                                xmlWriter.writeStartElement(prefix2,"Notas", namespace2);
                                                xmlWriter.writeNamespace(prefix2, namespace2);
                                                xmlWriter.setPrefix(prefix2, namespace2);

                                            } else {
                                                xmlWriter.writeStartElement(namespace2,"Notas");
                                            }

                                        } else {
                                            xmlWriter.writeStartElement("Notas");
                                        }


                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localNotas.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Notas"),
                                        factory,xmlWriter);
                                    }
                                } if (localAtributosTracker){
                                    if (localAtributos==null){

                                            java.lang.String namespace2 = "http://www.une.net.co/ncaInvM6";

                                        if (! namespace2.equals("")) {
                                            java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                            if (prefix2 == null) {
                                                prefix2 = generatePrefix(namespace2);

                                                xmlWriter.writeStartElement(prefix2,"Atributos", namespace2);
                                                xmlWriter.writeNamespace(prefix2, namespace2);
                                                xmlWriter.setPrefix(prefix2, namespace2);

                                            } else {
                                                xmlWriter.writeStartElement(namespace2,"Atributos");
                                            }

                                        } else {
                                            xmlWriter.writeStartElement("Atributos");
                                        }


                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localAtributos.serialize(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Atributos"),
                                        factory,xmlWriter);
                                    }
                                }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "FechaSolicitud"));
                            
                            
                                    if (localFechaSolicitud==null){
                                         throw new org.apache.axis2.databinding.ADBException("FechaSolicitud cannot be null!!");
                                    }
                                    elementList.add(localFechaSolicitud);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Direccion"));
                            
                            
                                    if (localDireccion==null){
                                         throw new org.apache.axis2.databinding.ADBException("Direccion cannot be null!!");
                                    }
                                    elementList.add(localDireccion);
                                
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "OfertaEconomica"));
                                 
                                        if (localOfertaEconomica != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfertaEconomica));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("OfertaEconomica cannot be null!!");
                                        }
                                     if (localDetalleRespuestaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "DetalleRespuesta"));
                            
                            
                                    elementList.add(localDetalleRespuesta==null?null:
                                    localDetalleRespuesta);
                                } if (localListaRFSTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "ListaRFS"));
                            
                            
                                    elementList.add(localListaRFS==null?null:
                                    localListaRFS);
                                } if (localListaRFSNoExistentesTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "ListaRFSNoExistentes"));
                            
                            
                                    elementList.add(localListaRFSNoExistentes==null?null:
                                    localListaRFSNoExistentes);
                                } if (localAccesosTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Accesos"));
                            
                            
                                    elementList.add(localAccesos==null?null:
                                    localAccesos);
                                } if (localNotasTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Notas"));
                            
                            
                                    elementList.add(localNotas==null?null:
                                    localNotas);
                                } if (localAtributosTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6",
                                                                      "Atributos"));
                            
                            
                                    elementList.add(localAtributos==null?null:
                                    localAtributos);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static M6ConsultarRFSType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            M6ConsultarRFSType object =
                new M6ConsultarRFSType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"M6ConsultarRFSType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (M6ConsultarRFSType)co.net.une.www.ncainvm6.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","FechaSolicitud").equals(reader.getName())){
                                
                                                object.setFechaSolicitud(co.net.une.www.ncainvm6.UTCDate.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Direccion").equals(reader.getName())){
                                
                                                object.setDireccion(co.net.une.www.ncainvm6.Direcciontype.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","OfertaEconomica").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOfertaEconomica(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","DetalleRespuesta").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setDetalleRespuesta(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setDetalleRespuesta(co.net.une.www.ncainvm6.Detallerespuestatype.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","ListaRFS").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setListaRFS(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setListaRFS(co.net.une.www.ncainvm6.Listarfstype.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","ListaRFSNoExistentes").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setListaRFSNoExistentes(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setListaRFSNoExistentes(co.net.une.www.ncainvm6.Listarfsnoexistentestype.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Accesos").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setAccesos(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setAccesos(co.net.une.www.ncainvm6.Listaaccesosrfstype.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Notas").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setNotas(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setNotas(co.net.une.www.ncainvm6.Listanotastype.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/ncaInvM6","Atributos").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setAtributos(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setAtributos(co.net.une.www.ncainvm6.Listaatributostype.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          